# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Puppet System. The API that was used to build the adapter for Puppet is usually available in the report directory of this adapter. The adapter utilizes the Puppet API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Puppet adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Puppet to automate infrastructure provisioning, configuration management and orchestration.

With this adapter you have the ability to perform operations with Puppet such as:

- Environments
- Node
- Monitoring and reporting

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
