# Puppet

Vendor: Puppet
Homepage: https://www.puppet.com/

Product: Puppet
Product Page: https://www.puppet.com/

## Introduction
We classify Puppet into the CI/CD domain as Puppet is used to centralize and automate configuration management.

"Puppet allows you to specify which software and configuration a system requires and then maintain that state after initial setup"

## Why Integrate
The Puppet adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Puppet to automate infrastructure provisioning, configuration management and orchestration.

With this adapter you have the ability to perform operations with Puppet such as:

- Environments
- Node
- Monitoring and reporting

## Additional Product Documentation
The [API documents for Puppet](https://www.puppet.com/docs/puppet/8/puppet_index.html)